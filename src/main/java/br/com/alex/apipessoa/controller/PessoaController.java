package br.com.alex.apipessoa.controller;

import br.com.alex.apipessoa.model.dto.PessoaDTO;
import br.com.alex.apipessoa.model.entity.Pessoa;
import br.com.alex.apipessoa.repository.PessoaRepository;
import br.com.alex.apipessoa.service.PessoaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/apiPessoa")
@AllArgsConstructor
public class PessoaController {

    private PessoaService pessoaService;
    private PessoaRepository pessoaRepository;

    @ApiOperation(value = "Retorna uma lista de pessoas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna a lista de pessoa"),
            @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
            @ApiResponse(code = 500, message = "Ocorreu uma exceção de erro interno"),
    })
    @GetMapping(value="/pessoas", produces="application/json")
    public ResponseEntity<List<PessoaDTO>> pessoas() {
        return new ResponseEntity<>(pessoaService.listarPessoas(), HttpStatus.OK);
    }

    @ApiOperation(value = "Retorna busca de pessoa pelo identificador")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna pessoa pelo identificador"),
            @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @GetMapping(value="/pessoas/{id}", produces="application/json", consumes="application/json")
    public ResponseEntity<PessoaDTO> pessoa(@PathVariable Long id) {
        Pessoa pessoaResponse = pessoaService.buscarPessoa(id);
        PessoaDTO pessoaDTO = new PessoaDTO();
        BeanUtils.copyProperties(pessoaResponse, pessoaDTO);
        return new ResponseEntity<>(pessoaDTO, HttpStatus.OK);
    }

    @PostMapping(value="/pessoa", produces="application/json", consumes="application/json")
    public ResponseEntity<PessoaDTO> pessoa(@RequestBody @Valid Pessoa pessoaRequest) {
        pessoaService.validarElementosEmDuplicidade(pessoaRequest);
        Pessoa pessoaResponse = pessoaService.cadastrarPessoa(pessoaRequest);
        PessoaDTO pessoaDTO = new PessoaDTO();
        BeanUtils.copyProperties(pessoaResponse, pessoaDTO);
        return new ResponseEntity<>(pessoaDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/pessoa")
    public ResponseEntity<PessoaDTO> alterarPessoaCompleta(@RequestBody @Valid Pessoa pessoaRequest) {
        Pessoa pessoaAlterada = pessoaService.alterarPessoa(pessoaRequest);
        PessoaDTO pessoaDTO = new PessoaDTO();
        BeanUtils.copyProperties(pessoaAlterada, pessoaDTO);
        return new ResponseEntity<>(pessoaDTO, HttpStatus.OK);
    }
    @PatchMapping(value="/pessoa/{id}")
    public ResponseEntity<PessoaDTO> alterarPessoaParcial(@PathVariable Long id, @RequestBody @Valid Map<String, Object> pessoaRequest) {
        Pessoa pessoaAlterada = pessoaService.alterarPessoa(id, pessoaRequest);
        PessoaDTO pessoaDTO = new PessoaDTO();
        BeanUtils.copyProperties(pessoaAlterada, pessoaDTO);
        return new ResponseEntity<>(pessoaDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{idPessoa}")
    public void deletarPessoa(@PathVariable Long idPessoa) {
        pessoaRepository.deleteById(idPessoa);
    }



}
