package br.com.alex.apipessoa.service;

import br.com.alex.apipessoa.exception.BuscaSemRetornoException;
import br.com.alex.apipessoa.exception.DuplicidadeElementoUnicoException;
import br.com.alex.apipessoa.exception.OperacaoBaseDadosException;
import br.com.alex.apipessoa.model.dto.PessoaDTO;
import br.com.alex.apipessoa.model.entity.Pessoa;
import br.com.alex.apipessoa.repository.PessoaRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class PessoaService {

    private PessoaRepository pessoaRepository;

    public List<PessoaDTO> listarPessoas() {
        return pessoaRepository.findAll().stream().map(PessoaDTO::criarPessoaDTO).collect(Collectors.toList());
    }

    public Pessoa buscarPessoa(Long id) {
        return pessoaRepository.findById(id)
                .orElseThrow(()-> new BuscaSemRetornoException("Pessoa não encontrada na base de dados"));
    }

    @Transactional
    public Pessoa cadastrarPessoa(Pessoa pessoaRequest) {
        Pessoa pessoa = null;
        try {
            pessoa = pessoaRepository.save(pessoaRequest);
        } catch(Exception e) {
            log.error("Erro ao salvar pessoa - {}", e.getMessage());
            throw new OperacaoBaseDadosException("Erro ao cadastrar pessoa");
        }
        return pessoa;
    }
    @Transactional
    public Pessoa alterarPessoa(Long id, Map<String, Object> pessoaRequest) {
        try {
            Optional<Pessoa> pessoaDB = pessoaRepository.findById(id);
            validarDuplicidadeCPF(pessoaRequest.get("cpf").toString());
            if (pessoaDB.isPresent()) {
                pessoaRequest.forEach((key, value)->{
                    log.info("key +********+ value");
                    log.info(key+" - "+value);
                    Field field = ReflectionUtils.findField(Pessoa.class, key);
                    field.setAccessible(true);
                    ReflectionUtils.setField(field, pessoaDB.get(), value);
                });
            }
            Pessoa pes = pessoaDB.get();
            return pessoaRepository.save(pes);
        } catch (Exception e) {
            log.error("\n*************** " + e.getMessage());
            throw e;
        }
    }

    @Transactional
    public Pessoa alterarPessoa(Pessoa pessoaRequest) {
        Optional<Pessoa> pessoaDB = pessoaRepository.findById(pessoaRequest.getId());
        validarElementosEmDuplicidade(pessoaRequest);
        if(pessoaDB.isPresent()) {
            return pessoaRepository.save(pessoaRequest);
        }
        throw new BuscaSemRetornoException("Pessoa não encontrada na base de dados");
    }

    public void validarElementosEmDuplicidade(Pessoa pessoaRequest) {
        validarDuplicidadeCPF(pessoaRequest.getCpf());
        validarDuplicidadeEmail(pessoaRequest.getEmail());
    }

    private void validarDuplicidadeCPF(String cpf) {
        Pessoa cpfExistenteDB = pessoaRepository.findByCpf(cpf);
        if (cpfExistenteDB != null) {
            throw new DuplicidadeElementoUnicoException("CPF já cadastrado na base de dados!");
        }
    }

    private void validarDuplicidadeEmail(String email) {
        try {
            Pessoa emailExistenteDB = pessoaRepository.findByEmail(email);
            if (emailExistenteDB != null) {
                throw new DuplicidadeElementoUnicoException("EMAIL já cadastrado na base de dados!");
            }
        } catch(Exception e) {
            log.error("Erro ao validar e-mail - {}", e.getMessage());
            throw new OperacaoBaseDadosException("Erro ao validar E-MAIL");
        }
    }

}
