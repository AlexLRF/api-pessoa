package br.com.alex.apipessoa.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Entity(name = "pessoa")
@Data
@ToString
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pessoa")
	private Long id;

	@ApiModelProperty(value = "Nome da pessoa")
	@Size(min = 2, max = 100, message = "Campo 'nome' deve ter de 2 a 100 caracteres")
	@Column(nullable = false)
	private String nome;

	@Size(min = 11, max = 11, message = "Campo 'cpf' deve ter 11 caracteres")
	@Column(nullable = false, unique = true)
	private String cpf;

	@Column(nullable = false, unique = true)
	private String email;

	@JsonProperty(value = "data_nascimento")
	@Column(name = "data_nascimento", nullable = false)
	private Date dataNascimento;

	@OneToOne(mappedBy = "pessoa", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Profissao profissao;

	@OneToOne(mappedBy = "pessoa", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Telefone telefones;

	@OneToOne(mappedBy = "pessoa", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Endereco endereco;

}
