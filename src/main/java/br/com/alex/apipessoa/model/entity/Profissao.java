package br.com.alex.apipessoa.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Entity(name = "profissao")
@Data
public class Profissao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_profissao")
	private Long id;

	@JsonProperty(value = "nome_profissao")
	@NotBlank(message = "Campo 'nomeProfissao' não pode ser vazio ou nulo")
	@Size(min = 2, max = 100, message = "Campo 'nomeProfissao' deve ter de 2 a 100 caracteres")
	@Column(name = "nome_profissao", nullable = false)
	private String nomeProfissao;

	@NotNull(message = "Campo 'clt' não pode ser vazio ou nulo")
	private Boolean clt;

	@JsonProperty(value = "renda_mensal")
	@NotNull(message = "Campo 'rendaMensal' não pode ser vazio ou nulo")
	@Column(name = "renda_mensal", nullable = false)
	private BigDecimal rendaMensal;

	@OneToOne
	@JoinColumn(name = "id_pessoa")
	@JsonBackReference
	private Pessoa pessoa;

}
