package br.com.alex.apipessoa.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Entity(name = "endereco")
@Data
public class Endereco  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_endereco")
	private Long id;

	@NotBlank(message = "Campo 'cep' não pode ser vazio ou nulo")
	@Size(min = 5, max = 10, message = "Campo 'cep' deve ter de 5 a 10 caracteres")
	@Column(nullable = false)
	private String cep;

	@JsonProperty(value = "numero_residencia")
	@NotBlank(message = "Campo 'numeroResidencia' não pode ser vazio ou nulo")
	@Size(min = 1, max = 10, message = "Campo 'numeroResidencia' deve ter de 1 a 10 caracteres")
	@Column(name = "numero_residencia", nullable = false)
	private String numeroResidencia;

	@Size(max = 30, message = "Campo 'complemento' deve ter de 1 a 30 caracteres")
	private String complemento;

	@NotBlank(message = "Campo 'logradouro' não pode ser vazio ou nulo")
	@Size(min = 2, max = 100, message = "Campo 'logradouro' deve ter de 2 a 100 caracteres")
	@Column(nullable = false)
	private String logradouro;

	@NotBlank(message = "Campo 'bairro' não pode ser vazio ou nulo")
	@Size(min = 2, max = 100, message = "Campo 'bairro' deve ter de 2 a 100 caracteres")
	@Column(nullable = false)
	private String bairro;

	@NotBlank(message = "Campo 'cidade' não pode ser vazio ou nulo")
	@Size(min = 2, max = 100, message = "Campo 'cidade' deve ter de 2 a 100 caracteres")
	@Column(nullable = false)
	private String cidade;

	@NotBlank(message = "Campo 'uf' não pode ser vazio ou nulo")
	@Size(min = 2, max = 2, message = "Campo 'uf' deve ter 2 caracteres")
	@Column(nullable = false)
	private String uf;

	@OneToOne
	@JoinColumn(name = "id_pessoa")
	@JsonBackReference
	private Pessoa pessoa;

}
