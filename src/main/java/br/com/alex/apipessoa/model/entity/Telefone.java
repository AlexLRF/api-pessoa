package br.com.alex.apipessoa.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Entity(name = "telefone")
@Data
public class Telefone  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_telefone")
	private Long id;

	@NotBlank(message = "Campo 'celular' não pode ser vazio ou nulo")
	@Size(max = 12, message = "Campo 'celular' deve ter no máximo 12 caracteres")
	@Column(nullable = false)
	private String celular;

	@Size(max = 12, message = "Campo 'telefoneFixo' deve ter no máximo 12 caracteres")
	@JsonProperty(value = "telefone_fixo")
	@Column(name = "telefone_fixo")
	private String telefoneFixo;

	@OneToOne
	@JoinColumn(name = "id_pessoa")
	@JsonBackReference
	private Pessoa pessoa;

}
