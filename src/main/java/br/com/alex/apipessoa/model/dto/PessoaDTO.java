package br.com.alex.apipessoa.model.dto;

import br.com.alex.apipessoa.model.entity.Endereco;
import br.com.alex.apipessoa.model.entity.Pessoa;
import br.com.alex.apipessoa.model.entity.Profissao;
import br.com.alex.apipessoa.model.entity.Telefone;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PessoaDTO implements Serializable{

	private Long id;

	@NotBlank(message = "Campo 'nome' não pode ser vazio ou nulo")
	@Size(min = 2, max = 100, message = "Campo 'nome' deve ter de 2 a 100 caracteres")
	private String nome;

	@NotBlank(message = "Campo 'cpf' não pode ser vazio ou nulo")
//	@CPF
	@Size(min = 11, max = 11, message = "Campo 'cpf' deve ter 11 caracteres")
	private String cpf;

	@NotBlank(message = "Campo 'email' não pode ser vazio ou nulo")
	@Email(message = "Campo 'email' deve conter um email válido")
	private String email;

	@JsonProperty(value = "data_nascimento")
	@NotNull(message = "Campo 'dataNascimento DTO' não pode ser vazio ou nulo")
	private Date dataNascimento;

	@JsonManagedReference
	private Profissao profissao;

	@JsonManagedReference
	@NotNull(message = "Campo 'telefone DTO' não pode ser vazio ou nulo")
	private Telefone telefones;

	@JsonManagedReference
	@NotNull(message = "Campo 'endereco' não pode ser vazio ou nulo")
	private Endereco endereco;

	public static PessoaDTO criarPessoaDTO(Pessoa pessoa) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(pessoa, PessoaDTO.class);
	}

	public static Pessoa criarPessoa(PessoaDTO pessoaDTO) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(pessoaDTO, Pessoa.class);
	}

}
