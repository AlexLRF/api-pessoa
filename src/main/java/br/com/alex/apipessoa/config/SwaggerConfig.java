package br.com.alex.apipessoa.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${URL_BASE}")
    private String urlBase;

    @Value("${PACOTE_BASE}")
    private String pacoteBase;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(this.pacoteBase))
                .paths(PathSelectors.any())
                .build()
                .host(this.urlBase)
                .apiInfo(definirInfoApi());
    }

    private ApiInfo definirInfoApi() {
        return new ApiInfoBuilder()
            .title("Pessoa API REST")
            .description("API REST de manutenção de cadastro de pessoa")
            .version("1.0.0")
            .license("Apache License Version 2.0")
            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
            .contact(new Contact(
                    "Alex Fogazzi",
                    "https://www.apache.org/licenses/LICENSE-2.0",
                    "alex.fogazzi.dev@gmail.com")
            )
            .build();
    }
}
