package br.com.alex.apipessoa.repository;

import br.com.alex.apipessoa.model.entity.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    Optional<Pessoa> findById(Long id);
    Pessoa findByCpf(String cpf);
    Pessoa findByEmail(String email);
}
