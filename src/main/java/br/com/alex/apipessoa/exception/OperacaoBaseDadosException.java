package br.com.alex.apipessoa.exception;

public class OperacaoBaseDadosException extends RuntimeException {

    public OperacaoBaseDadosException(String mensagem){
        super(mensagem);
    }
}
