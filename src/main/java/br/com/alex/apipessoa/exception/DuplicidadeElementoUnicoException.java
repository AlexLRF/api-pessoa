package br.com.alex.apipessoa.exception;

public class DuplicidadeElementoUnicoException extends RuntimeException {

    public DuplicidadeElementoUnicoException(String mensagem){
        super(mensagem);
    }
}
