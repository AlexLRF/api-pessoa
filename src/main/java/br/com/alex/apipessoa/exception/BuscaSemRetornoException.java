package br.com.alex.apipessoa.exception;

public class BuscaSemRetornoException extends RuntimeException {

    public BuscaSemRetornoException(String mensagem){
        super(mensagem);
    }
}
