package br.com.alex.apipessoa.exception;

import br.com.alex.apipessoa.model.dto.ErroValidacaoRequestDto;
import br.com.alex.apipessoa.model.dto.ResponseErroRequestDto;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

@RestControllerAdvice
@AllArgsConstructor
public class GlobalExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseErroRequestDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception){

        List<Object> errosDto = new ArrayList<>();

        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        fieldErrors.forEach(e -> {
            String mensagem = messageSource.getMessage(e, LocaleContextHolder.getLocale());
            ErroValidacaoRequestDto erro = new ErroValidacaoRequestDto(e.getField(), mensagem);
            errosDto.add(erro);
        });
        return new ResponseEntity<>(new ResponseErroRequestDto(HttpStatus.BAD_REQUEST.name(), errosDto,
                HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NoSuchElementException.class)
    public ResponseEntity<ResponseErroRequestDto> handleNoSuchElementException(NoSuchElementException exception) {
        return new ResponseEntity<>(
                new ResponseErroRequestDto(
                        HttpStatus.NOT_FOUND.name()
                        , List.of(exception.getMessage())
                        , HttpStatus.NOT_FOUND.value()
                )
                , HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = DuplicidadeElementoUnicoException.class)
    public ResponseEntity<ResponseErroRequestDto> handleDuplicidadeElementoUnicoException(DuplicidadeElementoUnicoException exception) {
        return new ResponseEntity<>(
                new ResponseErroRequestDto(
                        HttpStatus.BAD_REQUEST.name()
                        , List.of(exception.getMessage())
                        , HttpStatus.BAD_REQUEST.value()
                )
                , HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<ResponseErroRequestDto> handleConstraintViolationException(ConstraintViolationException exception) {
        List<Object> errosDto = new ArrayList<>();

        Set<ConstraintViolation<?>> fieldErrors = exception.getConstraintViolations();
        fieldErrors.forEach(e -> {
            String mensagem = e.getMessage();
            ErroValidacaoRequestDto erro = new ErroValidacaoRequestDto(null, mensagem);
            errosDto.add(erro);
        });
        return new ResponseEntity<>(new ResponseErroRequestDto(HttpStatus.BAD_REQUEST.name(), errosDto,
                HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OperacaoBaseDadosException.class)
    public ResponseEntity<ResponseErroRequestDto> handleOperacaoBaseDadosException(OperacaoBaseDadosException exception) {
        return new ResponseEntity<>(
                new ResponseErroRequestDto(
                        HttpStatus.INTERNAL_SERVER_ERROR.name()
                        , List.of(exception.getMessage())
                        , HttpStatus.INTERNAL_SERVER_ERROR.value()
                )
                ,HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(value = BuscaSemRetornoException.class)
    public ResponseEntity<ResponseErroRequestDto> handleBuscaSemRetornoException(BuscaSemRetornoException exception) {
        return new ResponseEntity<>(
                new ResponseErroRequestDto(
                        HttpStatus.NOT_FOUND.name()
                        , List.of(exception.getMessage())
                        , HttpStatus.NOT_FOUND.value()
                )
                ,HttpStatus.NOT_FOUND
        );
    }

}
